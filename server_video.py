import argparse
import asyncio
import logging
import sys
from aiortc import RTCPeerConnection, RTCSessionDescription, VideoStreamTrack
from aiortc.contrib.media import MediaRecorder
from aiortc.contrib.signaling import add_signaling_arguments, create_signaling
import cv2

class VideoStream(VideoStreamTrack):
    def __init__(self):
        super().__init__()
        self.cap = cv2.VideoCapture()

    async def recv(self):
        pts, time_base = await self.next_timestamp()
        ret, frame = self.cap.read()
        if not ret:
            return
        return frame

async def run(pc, recorder, signaling, role):
    await signaling.connect()

    if role == "answer":
        obj = await signaling.receive()
        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)
        await pc.setLocalDescription(await pc.createAnswer())
        sdp = pc.localDescription.sdp
        for line in sdp.split("\n"):
            sys.stdout.write(line + "\n")
            sys.stdout.flush()
        await signaling.send(pc.localDescription)

    async def consume_signaling():
        while True:
            obj = await signaling.receive()

            if isinstance(obj, RTCSessionDescription):
                await pc.setRemoteDescription(obj)

                if obj.type == "offer":
                    await pc.setLocalDescription(await pc.createAnswer())
                    sdp = pc.localDescription.sdp
                    for line in sdp.split("\n"):
                        sys.stdout.write(line + "\n")
                        sys.stdout.flush()
                    await signaling.send(pc.localDescription)
            elif obj is None:
                break

    @pc.on("track")
    async def on_track(track):
        print("Track received")
        if recorder:
            recorder.addTrack(track)

    await consume_signaling()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("role", choices=["offer", "answer"])
    parser.add_argument("--record-to", help="Write received media to a file.")
    parser.add_argument("--verbose", "-v", action="count")
    add_signaling_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    signaling = create_signaling(args)
    pc = RTCPeerConnection()

    video_stream = VideoStream()
    pc.addTrack(video_stream)

    if args.record_to:
        recorder = MediaRecorder(args.record_to)
    else:
        recorder = None

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run(pc, recorder, signaling, args.role))
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(signaling.close())
        loop.run_until_complete(pc.close())
