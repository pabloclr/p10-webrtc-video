import argparse
import asyncio
import logging
import sys
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaPlayer, MediaRecorder
from aiortc.contrib.signaling import add_signaling_arguments, create_signaling

async def run(pc, player, signaling, role):
    await signaling.connect()

    if player and player.video:
        pc.addTrack(player.video)

    if role == "offer":
        await pc.setLocalDescription(await pc.createOffer())
        sdp = pc.localDescription.sdp
        for line in sdp.split("\n"):
            sys.stdout.write(line + "\n")
            sys.stdout.flush()




    await pc.setLocalDescription(await pc.createOffer())
    await signaling.send(pc.localDescription)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("role", choices=["offer", "answer"])
    parser.add_argument("--play-from", help="Read the media from a file and send it.")
    parser.add_argument("--verbose", "-v", action="count")
    add_signaling_arguments(parser)
    args = parser.parse_args()
    role = sys.argv[0]

    signaling = create_signaling(args)
    pc = RTCPeerConnection()

    if args.play_from:
        player = MediaPlayer(args.play_from)
    else:
        player = None


    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run(pc, player, signaling, role))
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(signaling.close())
        loop.run_until_complete(pc.close())
