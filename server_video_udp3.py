import argparse
import asyncio
import logging
import json
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription, VideoStreamTrack
from aiortc.contrib.media import MediaBlackhole, MediaRecorder
from av import VideoFrame
import cv2

class VideoFileStream(VideoStreamTrack):
    def __init__(self, filepath):
        super().__init__()
        self.cap = cv2.VideoCapture(filepath)
        if not self.cap.isOpened():
            raise ValueError("Unable to open video file")

    async def recv(self):
        pts, time_base = await self.next_timestamp()
        ret, frame = self.cap.read()
        if not ret:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            ret, frame = self.cap.read()
            if not ret:
                raise EOFError("Cannot read video in the provided file")
        new_frame = VideoFrame.from_ndarray(frame, format='bgr24')
        new_frame.pts = pts
        new_frame.time_base = time_base
        return new_frame

class SignallingProtocol(asyncio.DatagramProtocol):
    def __init__(self, transport_send):
        self.transport_send = transport_send

    def connection_made(self, transport):
        self.transport = transport
        print("Server registered and waiting for messages.")
        transport.sendto("REGISTER SERVER".encode(), self.transport_send)

    def datagram_received(self, data, addr):
        message = data.decode()
        logging.info(f"Mensaje recibido de {addr}: {message}")

        try:
            message = json.loads(message)
        except json.JSONDecodeError:
            logging.error("Error decodificando el mensaje")
            return

        if message["type"] == "OFFER":
            offer = RTCSessionDescription(sdp=message["sdp"], type='offer')
            logging.info(f"Oferta SDP recibida: {offer.sdp}")
            video_file = message.get("file", "")
            if video_file:
                asyncio.ensure_future(self.handle_offer(offer, addr, video_file))
            else:
                logging.error("No video file specified in the offer")

    async def handle_offer(self, offer, addr, video_file):
        pc = RTCPeerConnection()
        recorder = MediaRecorder('received_video.mp4')

        @pc.on("track")
        async def on_track(track):
            print(f"Receiving {track.kind}")
            recorder.addTrack(track)
            await recorder.start()

        await pc.setRemoteDescription(offer)
        logging.info("Descripción remota configurada")

        video_track = VideoFileStream(video_file)
        pc.addTrack(video_track)
        logging.info("Pista de video añadida")

        answer = await pc.createAnswer()
        logging.info("Respuesta SDP creada")

        await pc.setLocalDescription(answer)
        logging.info("Descripción local configurada")
        response = json.dumps(
            {"type": "ANSWER", "sdp": pc.localDescription.sdp, "client_addr": addr}).encode()
        self.transport.sendto(response, addr)
        logging.info(f"Respuesta SDP enviada: {response}")

        # Enviar el mensaje BYE después de un tiempo
        await asyncio.sleep(10)  # Esperar un tiempo para enviar BYE después del video
        bye_message = json.dumps({"type": "bye"}).encode()
        self.transport.sendto(bye_message, addr)
        logging.info("Mensaje BYE enviado")

        await recorder.stop()
        await pc.close()

async def main():
    logging.info("Iniciando server")

    loop = asyncio.get_running_loop()
    _, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(('127.0.0.1', 9999)),
        local_addr=('127.0.0.1', 9900)
    )

    try:
        await asyncio.sleep(3600)  # Mantener el servidor corriendo
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.run(main())
