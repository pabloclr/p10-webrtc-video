import argparse
import asyncio
import logging
import json
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription, VideoStreamTrack
from aiortc.contrib.media import MediaBlackhole, MediaRecorder
from av import VideoFrame
import cv2
import sys

class VideoFileStream(VideoStreamTrack):
    def __init__(self, filepath):
        super().__init__()
        self.cap = cv2.VideoCapture(filepath)
        if not self.cap.isOpened():
            raise ValueError("Unable to open video file")

    async def recv(self):
        pts, time_base = await self.next_timestamp()
        ret, frame = self.cap.read()
        if not ret:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            ret, frame = self.cap.read()
            if not ret:
                raise EOFError("Cannot read video in the provided file")
        new_frame = VideoFrame.from_ndarray(frame, format='bgr24')
        new_frame.pts = pts
        new_frame.time_base = time_base
        return new_frame

class SignallingProtocol(asyncio.DatagramProtocol):
    def __init__(self, transport_send,server):
        self.transport_send = transport_send
        self.server_name = server
    def connection_made(self, transport):
        self.transport = transport
        print("Client registered and waiting for messages.")
        register_message = json.dumps({"type": "REGISTER SERVER", "server_name": self.server_name})
        transport.sendto(register_message.encode(), ('127.0.0.1', 9999))

    def datagram_received(self, data, addr):
        message = data.decode()
        logging.info(f"Mensaje recibido de {addr}: {message}")

        try:
            message = json.loads(message)
        except json.JSONDecodeError:
            logging.error("Error decodificando el mensaje")
            return

        if message["type"] == "OFFER":
            offer = RTCSessionDescription(sdp=message["sdp"], type='offer')
            logging.info(f"Oferta SDP recibida: {offer.sdp}")
            video_file = message.get("file", "")
            client_addr = message.get("addr", "")
            if video_file and client_addr:
                asyncio.ensure_future(self.handle_offer(offer, addr, video_file, client_addr))
            else:
                logging.error("No video file or client address specified in the offer")

    async def handle_offer(self, offer, addr, video_file, client_addr):
        pc = RTCPeerConnection()
        recorder = MediaRecorder('received_video.mp4')

        @pc.on("track")
        async def on_track(track):
            print(f"Receiving {track.kind}")
            recorder.addTrack(track)
            await recorder.start()

        await pc.setRemoteDescription(offer)
        logging.info("Descripción remota configurada")

        video_track = VideoFileStream(video_file)
        pc.addTrack(video_track)
        logging.info("Pista de video añadida")

        answer = await pc.createAnswer()
        logging.info("Respuesta SDP creada")

        await pc.setLocalDescription(answer)
        logging.info("Descripción local configurada")
        response = json.dumps({"type": "ANSWER", "sdp": pc.localDescription.sdp}).encode()
        client_ip, client_port = client_addr.split(":")
        self.transport.sendto(response, (client_ip, int(client_port)))
        logging.info(f"Respuesta SDP enviada: {response}")

        # Enviar el mensaje BYE después de un tiempo
        await asyncio.sleep(10)  # Esperar un tiempo para enviar BYE después del video
        bye_message = json.dumps({"type": "bye"}).encode()
        self.transport.sendto(bye_message, (client_ip, int(client_port)))
        logging.info("Mensaje BYE enviado")

        await recorder.stop()
        await pc.close()

async def main(server):
    logging.info("Iniciando server")

    loop = asyncio.get_running_loop()
    _, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(('127.0.0.1', 9999),server),
        local_addr=('127.0.0.1', 0)
    )

    try:
        await asyncio.sleep(3600)  # Mantener el servidor corriendo
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    server = str(sys.argv[1])
    asyncio.run(main(server))

