import asyncio
import json

clients = {}
servers = {}
pending_offers = {}

class SignallingProtocol(asyncio.DatagramProtocol):
    def connection_made(self, transport):
        self.transport = transport
        print("Signalling server started on 127.0.0.1:9999")

    def datagram_received(self, data, addr):
        message = data.decode()
        print(f"Received {message} from {addr}")

        try:
            msg_json = json.loads(message)
            if msg_json["type"] == "REGISTER SERVER" and "server_name" in msg_json:
                server_name = msg_json["server_name"]
                servers[server_name] = addr
                print(f"Servers: {servers}")
                print(f"Registered server {server_name} at {addr}")

                # Enviar ofertas pendientes
                if server_name in pending_offers:
                    for offer in pending_offers[server_name]:
                        self.transport.sendto(offer['data'], addr)
                    del pending_offers[server_name]

            elif msg_json["type"] == "REGISTER CLIENT" and "server_name" in msg_json:
                clients[addr] = {"server_name": msg_json["server_name"]}
                print(f"Clientes: {clients}")
                print(f"Registered client for {msg_json['server_name']} at {addr}")

            elif msg_json["type"] == "OFFER" and "server_name" in msg_json:
                server_name = msg_json["server_name"]
                if server_name in servers:
                    self.transport.sendto(data, servers[server_name])
                else:
                    print(f"Server {server_name} not registered. Storing offer.")
                    if server_name not in pending_offers:
                        pending_offers[server_name] = []
                    pending_offers[server_name].append({"data": data, "addr": addr})

            elif msg_json["type"] == "ANSWER" and "server_name" in msg_json:
                server_name = msg_json["server_name"]
                for client_addr, client_info in clients.items():
                    if client_info["server_name"] == server_name:
                        self.transport.sendto(data, client_addr)
                        break
        except json.JSONDecodeError as e:
            print(f"Failed to decode JSON: {e}")

async def main():
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(),
        local_addr=('127.0.0.1', 9999)
    )
    try:
        await asyncio.sleep(3600)  # Mantiene el servidor funcionando por 1 hora
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())
