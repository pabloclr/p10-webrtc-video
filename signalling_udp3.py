import asyncio
import json

clients = None
servers = None

class SignallingProtocol(asyncio.DatagramProtocol):
    def connection_made(self, transport):
        self.transport = transport
        print("Signalling server started on 127.0.0.1:9999")

    def datagram_received(self, data, addr):
        global clients, servers
        message = data.decode()
        print(f"Received {message} from {addr}")

        if message == "REGISTER CLIENT":
            clients = addr
            print(f"Registered client {addr}")
        elif message == "REGISTER SERVER":
            servers = addr
            print(f"Registered server {addr}")
        else:
            try:
                msg_json = json.loads(message)
                print(msg_json)
                if msg_json["type"] == "OFFER" and servers:
                    self.transport.sendto(data, servers)
                elif msg_json["type"] == "ANSWER" and clients:
                    self.transport.sendto(data, clients)
                elif msg_json["type"] == "bye":
                    self.transport.sendto(data, clients)
            except json.JSONDecodeError as e:
                print(f"Failed to decode JSON: {e}")

async def main():
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingProtocol(),
        local_addr=('127.0.0.1', 9999)
    )
    try:
        await asyncio.sleep(3600)  # Mantiene el servidor funcionando por 1 hora
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())
