import asyncio
import json
import logging
import sys
from aiortc import RTCPeerConnection, RTCSessionDescription, VideoStreamTrack
from aiortc.contrib.media import MediaPlayer, MediaRecorder, MediaBlackhole
from av import VideoFrame
import cv2

class VideoFileStream(VideoStreamTrack):
    def __init__(self, filepath):
        super().__init__()
        self.cap = cv2.VideoCapture(filepath)
        if not self.cap.isOpened():
            raise ValueError("Unable to open video file")

    async def recv(self):
        pts, time_base = await self.next_timestamp()
        ret, frame = self.cap.read()
        if not ret:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            ret, frame = self.cap.read()
            if not ret:
                raise EOFError("Cannot read video in the provided file")
        new_frame = VideoFrame.from_ndarray(frame, format='bgr24')
        new_frame.pts = pts
        new_frame.time_base = time_base
        return new_frame

class SignallingClient(asyncio.DatagramProtocol):
    def __init__(self, pc, transport_send, video_file, local_addr):
        self.pc = pc
        self.transport_send = transport_send
        self.video_file = video_file
        self.local_addr = local_addr

    def connection_made(self, transport):
        self.transport = transport
        print("Client registered and waiting for messages.")
        transport.sendto("REGISTER CLIENT".encode(), self.transport_send)
        asyncio.ensure_future(self.send_offer())

    def datagram_received(self, data, addr):
        message = data.decode()
        print(f"Received {message} from {addr}")
        try:
            msg_json = json.loads(message)
            if msg_json.get("type") == "ANSWER":
                asyncio.ensure_future(self.handle_answer(msg_json))
            elif msg_json.get("type") == "bye":
                print("Received BYE, shutting down.")
                asyncio.ensure_future(self.shutdown())
        except json.JSONDecodeError:
            print(f"Failed to decode JSON from {addr}: {message}")

    async def send_offer(self):
        video_track = VideoFileStream(self.video_file)
        self.pc.addTrack(video_track)
        await self.pc.setLocalDescription(await self.pc.createOffer())
        offer = json.dumps({"type": "OFFER", "sdp": self.pc.localDescription.sdp, "file": self.video_file, "addr": self.local_addr})
        self.transport.sendto(offer.encode(), self.transport_send)
        logging.info(f"Oferta SDP enviada: {offer}")

    async def handle_answer(self, msg_json):
        answer = RTCSessionDescription(sdp=msg_json["sdp"], type='answer')
        await self.pc.setRemoteDescription(answer)
        logging.info("Respuesta SDP configurada")

    async def shutdown(self):
        await self.pc.close()
        self.transport.close()
        await asyncio.sleep(1)
        asyncio.get_event_loop().stop()

async def main(video_file, signal_ip, signal_port, port):
    logging.basicConfig(level=logging.INFO)
    pc = RTCPeerConnection()

    local_addr = f"127.0.0.1:{port}"

    loop = asyncio.get_running_loop()
    _, protocol = await loop.create_datagram_endpoint(
        lambda: SignallingClient(pc, (signal_ip, signal_port), video_file, local_addr),
        local_addr=('127.0.0.1', port)
    )
    try:
        await asyncio.sleep(3600)  # Mantener el cliente corriendo
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    if len(sys.argv) <= 4:
        print(f"Usage: {sys.argv[0]} <video_file> <signal_ip> <signal_port> <port>")
        sys.exit(1)

    video_file = sys.argv[1]
    signal_ip = sys.argv[2]
    signal_port = int(sys.argv[3])
    port = int(sys.argv[4])

    asyncio.run(main(video_file, signal_ip, signal_port, port))
